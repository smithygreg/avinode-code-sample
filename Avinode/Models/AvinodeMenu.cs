﻿using System.Xml.Serialization;
using System.Collections.Generic;
namespace Avinode
{
    [XmlRoot(ElementName = "path")]
    public class Path
    {
        [XmlAttribute(AttributeName = "value")]
        public string Value { get; set; }
        [XmlAttribute(AttributeName = "system")]
        public string System { get; set; }
        [XmlAttribute(AttributeName = "theme")]
        public string Theme { get; set; }
        [XmlAttribute(AttributeName = "secure")]
        public string Secure { get; set; }
        [XmlAttribute(AttributeName = "context")]
        public string Context { get; set; }
    }

    [XmlRoot(ElementName = "item")]
    public class Item
    {
        public bool Active { get; set; }
        [XmlElement(ElementName = "displayName")]
        public string DisplayName { get; set; }
        [XmlElement(ElementName = "path")]
        public Path Path { get; set; }
        [XmlElement(ElementName = "subMenu")]
        public SubMenu SubMenu { get; set; }
        [XmlAttribute(AttributeName = "superOverride")]
        public string SuperOverride { get; set; }
    }

    [XmlRoot(ElementName = "subMenu")]
    public class SubMenu
    {
        [XmlElement(ElementName = "item")]
        public List<Item> Item { get; set; }
    }

    [XmlRoot(ElementName = "menu")]
    public class AvinodeMenu
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
        [XmlElement(ElementName = "item")]
        public List<Item> Item { get; set; }
    }

}
