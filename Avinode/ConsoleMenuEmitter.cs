﻿using System;

namespace Avinode
{
    public class ConsoleMenuEmitter : IMenuEmitter
    {
        public void ShowMenu(AvinodeMenu results)
        {
            foreach (var item in results.Item)
                ShowMenuItem(item, "");
        }

        private void ShowMenuItem(Item item, string indent)
        {
            Console.WriteLine(indent + item.DisplayName + ", " + item.Path.Value + (item.Active ? " ACTIVE" : ""));
            if(item.SubMenu!=null)
            {
                foreach (var sub in item.SubMenu.Item)
                {
                    ShowMenuItem(sub, indent + "\t");
                }
            }
        }
    }
}
