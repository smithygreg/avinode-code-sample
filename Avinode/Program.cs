﻿using System;

namespace Avinode
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 2)
            {
                DisplayArgumentErrorMessage();
            }
            else
            {
                try
                {
                    string menucontents = System.IO.File.ReadAllText(args[0]);
                    IMenuProcessor processor = new XMLMenuProcessor();
                    var results = processor.Process(menucontents, args[1]);
                    IMenuEmitter emitter = new ConsoleMenuEmitter();
                    emitter.ShowMenu(results);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        

        private static void DisplayArgumentErrorMessage()
        {
            Console.WriteLine("Error : You must supply two arguments to this program.");
            Console.WriteLine("ARGUMENTS : ");
            Console.WriteLine("1.\tFile name to be parsed");
            Console.WriteLine("2.\tXML Path to match");
        }
    }
}
