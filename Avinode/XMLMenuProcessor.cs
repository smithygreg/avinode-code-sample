﻿using System.IO;
using System.Xml.Serialization;

namespace Avinode
{
    public class XMLMenuProcessor : IMenuProcessor
    {

        AvinodeMenu IMenuProcessor.Process(string menusource, string pathtomatch)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(AvinodeMenu));
            TextReader reader = new StringReader(menusource);
            var menu = (AvinodeMenu)serializer.Deserialize(reader);
            reader.Close();

            if(menu.Item!=null)
            {
                foreach (var item in menu.Item)
                {
                    item.Active = GetMenuIsActive(item, pathtomatch);
                }
            }
            
            return menu;
        }

        private static bool GetMenuIsActive(Item item, string path)
        {
            bool ret = false;
            if (item.Path.Value == path)
                ret = true;
            else
            {
                if (item.SubMenu != null)
                {
                    foreach (var sub in item.SubMenu.Item)
                    {
                        sub.Active = GetMenuIsActive(sub, path);
                        ret = ret || sub.Active;
                    }
                }
            }
            return ret;
        }
    }
}
