﻿namespace Avinode
{
    public interface IMenuProcessor
    {
        AvinodeMenu Process(string menusource, string pathtomatch);
    }
}
