﻿using System;

namespace Avinode
{
    public interface IMenuEmitter
    {
        void ShowMenu(AvinodeMenu results);
    }
}
